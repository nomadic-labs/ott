(**************************************************************************)
(*                                 Ott                                    *)
(*                    Michelson-specific operations                       *)
(*                                                                        *)
(*                       Basile Pesin, Nomadic Labs                       *)
(**************************************************************************)

open Types

(** Retrieve all instruction names from the grammar definition
    (productions that begins with i_ and have the following character fully uppercase) *)

let instruction_ntr : nontermroot = "instruction"
let function_ntr : nontermroot = "function"
let instruction_prod_prefix : string = "i"
let type_prod_prefix : string = "ty"
let comparable_type_prod_prefix : string = "scty"

let typing_rule_prefix : string = "t"
let instruction_typing_rule_prefix : string = "t_instr"
let instruction_bigstep_rule_prefix : string = "bs"

let rule_regexp prefix name = Str.regexp (prefix^"_"^name^"\\(__.*\\)?$")

(* string stuff *)

let re_contains re s =
  try ignore (Str.search_forward re s 0); true
  with _ -> false

type case = Upper | Lower | Mixed

let pp_case = function
  | Upper -> "upper"
  | Lower -> "lower"
  | Mixed -> "mixed"

let get_case s =
  if (s = String.uppercase_ascii s) then Upper
  else if (s = String.lowercase_ascii s) then Lower
  else Mixed

let has_case c s = get_case s = c

let name_prefix (prod_name : string) : string = List.hd (String.split_on_char '_' prod_name)

let name_suffix (prod_name : string) : string = String.concat "_" (List.tl (String.split_on_char '_' prod_name))

let has_prefix p s = String.sub s 0 (min (String.length p) (String.length s)) = p

let prodname_is_instruction (pn : prodname) : bool =
  name_prefix pn = instruction_prod_prefix &&
    has_case Upper (name_suffix pn)

let instr_prodname_to_name (pn : prodname) : string =
  name_suffix pn

let ty_prodname_to_name (pn : prodname) : string =
  name_suffix pn

let dbg_prodname pn =
  Printf.printf "Prodname '%s'. Prefix: '%s' (case %s), Suffix: '%s' (case %s)\n"
    pn
    (name_prefix pn) (pp_case (get_case (name_prefix pn)))
    (name_suffix pn) (pp_case (get_case (name_suffix pn)))

(* michelson specific *)

let concat_map f l = List.concat (List.map f l)

let get_all_productions_of_ntr
      (xd : syntaxdefn)
      (ntr : nontermroot) : prod list  =
  let rule = Auxl.rule_of_ntr xd ntr in rule.rule_ps

let get_all_productions
      (xd : syntaxdefn)
      (entries : structure_entry list)
      (prefix : string) (case : case) : (prod * nontermroot) list  =
  concat_map (function
      | Struct_rs ntrs ->
         (* (ntr * rule) list *)
         let rs = List.map (fun ntr -> (ntr, Auxl.rule_of_ntr xd ntr)) ntrs in
         concat_map (fun (ntr, rule) ->
             (* get productions of this ntr *)
             let productions = rule.rule_ps in
             (* filter to obtain matching *)
             let productions_f =
               List.filter (fun p ->
                   let s = p.prod_name in
                   (has_prefix prefix s) && (get_case (name_suffix s) = case) && (not p.prod_meta)
                 ) productions in
             List.map (fun p -> (p, ntr)) productions_f
           ) rs
      (* (List.map (fun s ->
       *   {s with prod_name = name_suffix s.prod_name})
       *    (List.filter (fun p ->
       *      let s = p.prod_name in
       *      (name_prefix s = prefix) && (get_case (name_suffix s) = case) && (not p.prod_meta))
       *       (List.flatten (List.map (fun r -> r.rule_ps) rs)))) *)
      | _ -> []) entries


let get_all_instruction xd entries : (prod * nontermroot) list =
  get_all_productions xd entries instruction_prod_prefix Upper

let get_all_types xd entries : (prod * nontermroot) list =
  let tys = get_all_productions xd entries type_prod_prefix Lower in
  let ctys = get_all_productions xd entries comparable_type_prod_prefix Lower in
  (* let to_ty comp p = { production = p; comparable = comp; } in *)
  tys @ ctys

let type_is_comparable ((p, ntr) : prod * nontermroot) : bool =
  has_prefix comparable_type_prod_prefix p.prod_name

(** Retrieve all relations *)
let rec get_all_relations l =
  Auxl.option_map
    (function
     | RDC dc ->
        let semiraw_rules = List.flatten (List.map (fun def -> def.d_rules) dc.dc_defns) in
        let drules = Auxl.option_map (fun r -> match r with PSR_Rule d -> Some d |  _ -> None) semiraw_rules in
        Some drules
     | _ -> None) l |>
    List.concat

(** Retrieve all typing rules (that begins with t_) *)
let get_all_typingrules rels =
  List.filter (fun d -> has_prefix typing_rule_prefix d.drule_name) (get_all_relations rels)

(** Retrieve all bigstep rules (that begins with bs_) *)
let get_all_bigsteprules rels =
  List.filter (fun d -> has_prefix instruction_bigstep_rule_prefix d.drule_name) (get_all_relations rels)


(* collect all the nodes in a symterm *)
let rec nodes_of_symterm st : symterm list =
  match st with
  | St_node (_,stnb) -> st :: nodes_of_symterm_node_body stnb
  | St_nonterm (_,_,nt) -> []
  | St_nontermsub (_,ntrl,ntrt,nt) -> []
  | St_uninterpreted (_,_)-> []
and nodes_of_symterm_element ste =
  match ste with
  | Ste_st (_,st) -> nodes_of_symterm st
  | Ste_metavar (_,_,mv) -> [ ]
  | Ste_var _ -> []
  | Ste_list (_,stlis)  ->
      List.concat (List.map nodes_of_symterm_list_item stlis)
and nodes_of_symterm_list_item stli =
  match stli with
  | Stli_single(_,stes) ->
      List.concat (List.map nodes_of_symterm_element stes)
  | Stli_listform stlb ->
      nodes_of_symterm_list_body stlb
and nodes_of_symterm_node_body stnb =
  List.concat (List.map nodes_of_symterm_element stnb.st_es)
and nodes_of_symterm_list_body stlb =
  List.concat (List.map nodes_of_symterm_element stlb.stl_elements)
and nodes_of_symterms sts =
  List.concat (List.map nodes_of_symterm sts)

(* collect all the nonterms/metavars in a symterm *)
let rec prods_of_symterm st : prodname list =
  match st with
  | St_node (_,stnb) -> stnb.st_prod_name :: prods_of_symterm_node_body stnb
  | St_nonterm (_,_,nt) -> []
  | St_nontermsub (_,ntrl,ntrt,nt) -> []
  | St_uninterpreted (_,_)-> []
and prods_of_symterm_element ste =
  match ste with
  | Ste_st (_,st) -> prods_of_symterm st
  | Ste_metavar (_,_,mv) -> [ ]
  | Ste_var _ -> []
  | Ste_list (_,stlis)  ->
      List.concat (List.map prods_of_symterm_list_item stlis)
and prods_of_symterm_list_item stli =
  match stli with
  | Stli_single(_,stes) ->
      List.concat (List.map prods_of_symterm_element stes)
  | Stli_listform stlb ->
      prods_of_symterm_list_body stlb
and prods_of_symterm_node_body stnb =
  List.concat (List.map prods_of_symterm_element stnb.st_es)
and prods_of_symterm_list_body stlb =
  List.concat (List.map prods_of_symterm_element stlb.stl_elements)
and prods_of_symterms sts =
  List.concat (List.map prods_of_symterm sts)

(** Filter rules according to their names *)
let filter_rules_by_prod
      (xd : syntaxdefn)
      (prefix : string)
      ((prod, prod_ntr) : prod * nontermroot)
      (rules : drule list) =
  (** hacky solution:
   **   a rule is related to the production if
   **     1. it contains prod directly or
   **     2. ntr != instruction, function & it contains ntr (using prods_of_symterm)
   ***)
  (* print_endline ("Finding rules for " ^ prod.prod_name ^ " of ntr " ^ prod_ntr ^ " with prefix " ^ prefix ^ ": ") ; *)
  (* List.iter (fun r -> print_endline r.drule_name) bigsteprules; *)
  (* let rec ancestors_aux l p =
   *   let p' = Auxl.promote_ntr xd p in
   *   print_endline ("ancestor of " ^ p ^ " -> " ^ p') ;
   *   if p' = p then l
   *   else ancestors_aux (p' :: l) p' in
   * let ancestors p = ancestors_aux [p] p in
   * let ancestors_prod = ancestors prod_ntr in
   * print_endline "Ancestors: " ;
   * List.iter print_endline ancestors_prod;
   * let relatives a1 a2 = List.exists (fun e -> List.mem e a2) a1 in
   * let rec left_most_nt : symterm -> string option = function
   *   | St_node ([], {st_es = [Ste_st ([], (St_node _ as node)); _]}) -> left_most_nt node
   *   | St_node ([], ({st_es = [Ste_st ([], (St_nonterm _)); _]} as node_body)) -> Some node_body.st_prod_name
   *   | _ -> None
   * in *)
  let rules = List.filter (fun r ->
                  (* print_endline ("rule " ^ r.drule_name ^ " has prefix " ^ (prefix ^ "_" ^ prod.prod_name)) ; *)
                  let instr = (String.sub prod.prod_name 2 (String.length prod.prod_name - 2)) in
                  Str.string_match (Str.regexp (prefix ^ "_" ^ instr ^ "\\(__.*\\)?$")) r.drule_name 0
                  (* has_prefix (prefix ^ "_" ^ ) r.drule_name *)
                ) rules in

  (* let rules = List.filter (fun r ->
   *     (\* print_endline (" - " ^ r.drule_name) ; *\)
   *     (\* List.iter (fun s -> print_endline ("   + " ^ s)) (List.map fst (Auxl.nts_of_symterm r.drule_conclusion)); *\)
   *     List.mem prod.prod_name (prods_of_symterm r.drule_conclusion) ||
   *       (prod_ntr <> instruction_ntr &&
   *        prod_ntr <> function_ntr &&
   *          List.mem prod_ntr (List.map fst (Auxl.nts_of_symterm r.drule_conclusion)))
   * ) rules in *)
  (* if [] = rules then
   *   print_endline ("Warning: found no rules for " ^ prod.prod_name)
   * else
   *   List.iter (fun r -> print_endline (" found " ^ r.drule_name)) rules; *)
  rules

let instr_of_typing_rule r : string option =
  if (Str.string_match (rule_regexp instruction_typing_rule_prefix "\\([A-Z_]*?\\)") r.drule_name 0) then
    Some (Str.matched_group 1 r.drule_name)
  else
    None

(* hack: a typing rule "relates" to a michelson operation when its
 * conclusion contains the type, i.e. as input or output.
 *
 * then, find the typed instruction from the conclusions by inspecting a fixed position (specifically, 4th production):
 *   if it is production that follows instruction naming convention, return that production
 *   if it is production that does not follow instruction case, return all productions of its first argument
 *     - i.e. (i_unary_comparison comparison) -> returns all prodcution of comparison
 *     - i.e. (i_hash_function hash_comparison) -> returns all prodcution of hash_comparison
 *
 * *)
let instr_prod_of_typing_rule (r : drule) : symterm_node_body option  =
  let p = List.nth (nodes_of_symterm r.drule_conclusion) 3 in
  match p with
  | St_node (_, ({st_rule_ntr_name = ntr} as stnb)) ->
     (match ntr with
     | "instruction" | "function" -> Some stnb
     | _ ->
        (* print_endline ntr; *)
        None)
  | _ -> None

let get_type_related_ops (sd : systemdefn) (ty_prod, ty_ntr : prod * nontermroot ) : prodname list =
  (* print_endline (" - Find related instructions of " ^ ty_prod.prod_name) ; *)
  concat_map
    (fun d ->
      (* print_endline ("   + Typing rule " ^ d.drule_name) ;
       * print_endline ("      * Nts: " ^ (String.concat ", " (List.map fst (Auxl.nts_of_symterm d.drule_conclusion))));
       * print_endline ("      * Prods: " ^ (String.concat ", " (prods_of_symterm d.drule_conclusion))); *)
      if List.mem ty_prod.prod_name (prods_of_symterm d.drule_conclusion)
      then
        let matches = match instr_prod_of_typing_rule d with
        | Some i ->
           if prodname_is_instruction i.st_prod_name
           then [i.st_prod_name]
           else (
             (* (print_endline ("       -> " ^ i.st_prod_name ^ " does not match instr pattrn"); *)
              (* dbg_prodname i.st_prod_name; *)
              match i with
              | {st_es = [Ste_st (_, St_nonterm (_, ntr_arg, _))]} ->
                 List.map (fun p -> p.prod_name) (get_all_productions_of_ntr sd.syntax ntr_arg)
              | _ ->
                 (* print_endline (" Ignoring " ^ d.drule_name); *)
                 [])
        | _ -> [] in
        (* print_endline ("       -> Matches: " ^ (String.concat ", " matches)); *)
        matches
      else []
    ) (get_all_typingrules sd.relations) |> List.sort_uniq String.compare


(** Pretty printing functions *)

(* Define an ascii_mode *)
let ascii_mode = (Ascii {ppa_colour = false;
                         ppa_ugly = false;
                         ppa_lift_cons_prefixes = false;
                         ppa_show_deps = false;
                         ppa_show_defns = false; })

(* print production with arguments e.g. "IF_CONS bt bf" *)
let pp_ascii_prod_args sd p =
  String.escaped (Auxl.option (Grammar_pp.pp_elements ascii_mode
                                 sd.syntax [] p.prod_es false false false false) "")

(* Pretty print a symbolic term to ascii, and add some Michelson
 * specific output conversion.
 **)
let pp_ascii_symterm sd symterm =
  (* list of syntactical constructs to filter from ascii output *)
  let st_ascii =
    (String.escaped
       (Grammar_pp.pp_symterm ascii_mode sd.syntax [] de_empty
          symterm)) in
  let replace_all replacements str =
    List.fold_left
      (fun str' (needle, replacement) ->
        Str.global_replace needle replacement str')
      str replacements in
  replace_all [(Str.regexp "to_type ", "")] st_ascii

